import mysql.connector
class Repository():
     
    def __init__(self):
        self.cnx = mysql.connector.connect(user='root', password='Cavalieri3110!',
                                                 host='127.0.0.1',
                                                 database='sakila',
                                                 auth_plugin='mysql_native_password')

        self.cursor = self.cnx.cursor()
        self.cursor.execute("DROP VIEW tarefas_nao_feitas;")
        self.cursor.execute("DROP VIEW tarefas_feitas")

        self.cursor.execute("CREATE VIEW tarefas_nao_feitas as SELECT sdt.name , p.name  as 'nome_pessoa' , sp.name as 'Sprint Name', sp.startDate, sp.endDate FROM scrum_intented_development_task sidt inner join scrum_development_task sdt on sdt.id = sidt.id inner join team_member_scrum_development_task tmsdt on tmsdt.scrum_development_task_id = sidt.id inner join team_member tm on tm.id = tmsdt.team_member_id inner join person p on p.id  = '22' inner join sprint_backlog_scrum_development_task sbsdt on sbsdt.scrum_development_task_id  = sdt.id inner join sprint_backlog sb on sb.id = sbsdt.sprint_backlog_id inner join sprint sp on sp.uuid = '4070680f77d44da293b252ab3003967f' WHERE sidt.id NOT IN (SELECT caused_by FROM scrum_performed_development_task);")
        self.cursor.execute("CREATE VIEW tarefas_feitas as SELECT sdt.name , p.name  as 'nome_pessoa' , sp.name as 'Sprint Name', sp.startDate, sp.endDate FROM scrum_intented_development_task sidt inner join scrum_development_task sdt on sdt.id = sidt.id inner join team_member_scrum_development_task tmsdt on tmsdt.scrum_development_task_id = sidt.id inner join team_member tm on tm.id = tmsdt.team_member_id inner join person p on p.id  = '22' inner join sprint_backlog_scrum_development_task sbsdt on sbsdt.scrum_development_task_id  = sdt.id inner join sprint_backlog sb on sb.id = sbsdt.sprint_backlog_id inner join sprint sp on sp.uuid = '4070680f77d44da293b252ab3003967f' WHERE sidt.id IN (SELECT closed_by FROM scrum_performed_development_task);")                                        
        
    def tarefas_nao_feitas(self):
        
        self.cursor.execute("SELECT * FROM tarefas_nao_feitas")
        resultado = self.cursor.fetchall()
        
        return resultado

        self.cursor.close()

    def tarefas_feitas(self):
        self.cursor.execute("SELECT * FROM tarefas_feitas")
        resultado = self.cursor.fetchall()

        return resultado
        
        self.cursor.close()
    