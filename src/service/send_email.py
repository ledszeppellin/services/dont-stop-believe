import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
import email.message
from email import encoders
from jinja2 import FileSystemLoader, Environment
from pprint import pprint
from unidecode import unidecode


class SendEmail: 

    def __init__(self):

        self.fromaddr = "maulazdev@gmail.com"
        self.toaddr = ["dyego.cavalieri.dc@gmail.com"]

    def _send_email(self,email_content, subject):
        msg = MIMEMultipart("alternative")
        msg['From'] = self.fromaddr
        msg['To'] = ','.join(self.toaddr)
        msg['Subject'] = subject

        msg.add_header('Content-Type', 'text/html')
        part1 = MIMEText(email_content, "html", "utf-8")
        msg.attach(part1)
                    
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login(self.fromaddr, "52026624Ch!")
        text = msg.as_string()
        s.sendmail(self.fromaddr, self.toaddr, text)
        s.quit()

    def __load_file_developers(self, data, template_file):
        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)
        template = env.get_template(template_file)
        x =  template.render(user_nao_feitas=data["user_nao_feitas"], 
                                                    sprint_nao_feitas=data["sprint_nao_feitas"],
                                                    tasks_nao_feitas=data["tasks_nao_feitas"]).encode("utf-8")
        return str(x.decode("utf-8"))
   
   
    def __load_file_master(self, data, template_file):
        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)
        template = env.get_template(template_file)
        x =  template.render(user_feitas=data["user_feitas"], 
                                                    sprint_feitas=data["sprint_feitas"],
                                                    tasks_feitas=data["tasks_feitas"]).encode("utf-8")
        return str(x.decode("utf-8"))
    
    def send_wip_notification_developers(self, data):
        
        subject = "WIP Notification"
        template_file = "wip_developers.txt"
        email_content = self.__load_file_developers(data,template_file)
        self._send_email(email_content,subject)

    def send_wip_notification_master(self, data):
        
        subject = "WIP Notification"
        template_file = "wip_scrum_master_product_onwer.txt"
        email_content = self.__load_file_master(data,template_file)
        self._send_email(email_content,subject)    
        
        