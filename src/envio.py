from jinja2 import Template
from model.models import User,Project,Task, Sprint
from service.send_email import SendEmail
from repository.repository import Repository
from datetime import date, datetime


repository = Repository()
tarefas_nao_feitas = repository.tarefas_nao_feitas()
tarefas_feitas = repository.tarefas_feitas()

project = Project("Project X")
tasks_nao_feitas = []
sprint_nao_feitas = []
user_nao_feitas = []
for linha in tarefas_nao_feitas:
    tasks_nao_feitas.append(Task(linha[0],linha[3].strftime('%d/%m/%Y')))
    sprint_nao_feitas.append(Sprint(linha[2], linha[3].strftime('%d/%m/%Y'), linha[4].strftime('%d/%m/%Y')))
    user_nao_feitas.append(User(linha[1],""))

tasks_feitas = []
sprint_feitas = []
user_feitas = []
for linha in tarefas_feitas:
    tasks_feitas.append(Task(linha[0],linha[3].strftime('%d/%m/%Y')))
    sprint_feitas.append(Sprint(linha[2], linha[3].strftime('%d/%m/%Y'), linha[4].strftime('%d/%m/%Y')))
    user_feitas.append(User(linha[1],""))


data = {"user_nao_feitas": user_nao_feitas, "project":project, "sprint_nao_feitas":sprint_nao_feitas, 
            "tasks_nao_feitas": tasks_nao_feitas}

data1 = {"user_feitas": user_feitas, "project":project, "sprint_feitas":sprint_feitas, 
            "tasks_feitas": tasks_feitas}

send = SendEmail()
send.send_wip_notification_developers(data)
send.send_wip_notification_master(data1)
